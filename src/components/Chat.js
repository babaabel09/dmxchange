import { Link } from "react-router-dom";
import { useState, useEffect } from "react";

const Chat = (props) => {
  const [openChat, setOpenChat] = useState(false);
  const toggleChat = props.toggleChat;
  // console.log(x());
  const handleChat = () => {
    setOpenChat(!openChat);
  };

  useEffect(() => {
    if (window.location.pathname === "/contact") {
      setOpenChat(!openChat);
    }
  }, []);

  return (
    <div className="chat flex flex-col">
      <Link to="mailto: dollamanwealth@gmail.com">
        <i
          className={`${
            openChat ? "showmailChatCons" : ""
          } mailCon  bi bi-envelope-at`}
        ></i>
      </Link>
      {/* showmailChatCons */}
      {/* showWhatsappChatCons */}
      <Link to="https://wa.link/434i4e" className="font-semibold text-white ">
        <i
          className={` ${
            openChat ? "showWhatsappChatCons" : ""
          } whatsappCon bi bi-whatsapp`}
        ></i>
      </Link>
      <i onClick={handleChat} className=" chatCon bi bi-chat-dots"></i>
    </div>
  );
};

export default Chat;
